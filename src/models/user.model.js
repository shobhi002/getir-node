const mongoose = require('mongoose');

const userSchema = mongoose.Schema(
  {
    key: {
      type: String,
    },
    value: {
      type: String,
    },
    createdAt:{
      type: Date
    },
    counts:{
      type: Array
    }
  },
  {
    timestamps: true,
  }
);

/**
 * @typedef User
 */
const User = mongoose.model('records', userSchema);

module.exports = User;
