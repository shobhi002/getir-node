const httpStatus = require('http-status');
const { User } = require('../models');
const ApiError = require('../utils/ApiError');



/**
 * Query for users
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const getData = async (filter, options) => {
  const users = 
  // User.find({}, options);
  await User.aggregate(
    [
      {
        $project:{
          _id:0,
          totalCount:{$sum:"$counts"},
          key:1,
          createdAt:1
        }
      },
      { $match: {
        $and: [{totalCount:{ $gte: 2700 }},{totalCount:{ $lte: 3000 }}]
        

    } }
    ]
  );
  return users;
};


module.exports = {
  getData
};
