const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { dataService } = require('../services');
const { min } = require('moment');

const getData = catchAsync(async (req, res) => {
  console.log("aaaaaa");
  let {startDate, endDate, minCount, maxCount} = req.body;
let options = {startDate, endDate, minCount, maxCount};
  const user = await dataService.getData(options);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  res.send(user);
});

module.exports = {
  getData,
};
